#!/bin/bash

##############################################################################################
if [[ $1 != 1 ]]; then
    "$PWD/glade2script.py"  -g "$PWD/sylaba2.glade"  -s "$0 1" -d -t "@@t1@@|FONT|FORE" -t "@@t2@@|FONT|FORE" --combobox "@@_c1@@col
consonnes
CONSONNES
voyelles
VOYELLES" --combobox "@@_c2@@col
voyelles
VOYELLES
consonnes
CONSONNES" --combobox "@@_audioplayer@@col
MPlayer (par default)
Sox
GStreamer
VLC
OdyO (Expérimental)"
    exit
fi
PID=$$
FIFO=/tmp/FIFO${PID}
mkfifo $FIFO
###############################################################################################
arg1=""
arg2=""
T1="consonnes"
T2="voyelles"
C1_=""
C2_=""
C1=""
C2=""
NP="start"
ENGINE="mplayer "
P1=3301
P2=3302
P3=3303

function get_font() {
    cat $PWD/listes/$T2 | while read line; do
        FONTLABEL=`echo $line | cut -d '|' -f2 | sed 's/\ [0-9]*$//g'`
        if [[ "$FONTLABEL" != "" ]]; then
            echo -n "${FONTLABEL} 64"
            break
        else
            echo -n "sans sherif 64"
            break
        fi
    done
}

FONTLABEL=`get_font`
cat $PWD/listes/$T1 | while read line; do
    C1=`echo $line | cut -d '|' -f3 `
    F1=`echo $line | cut -d '|' -f2 `
    echo "SET@_colorC1.set_color(Gdk.color_parse('$C1'))"
    echo "SET@_fontC1.set_font('$F1')"
    echo "SET@_fontC1.set_preview_text('Consonnes')"
    break
done
cat $PWD/listes/$T2 | while read line; do
    C2=`echo $line | cut -d '|' -f3 `
    F2=`echo $line | cut -d '|' -f2 `
    echo "SET@_colorC2.set_color(Gdk.color_parse('$C2'))"
    echo "SET@_fontC2.set_font('$F2')"
    echo "SET@_fontC2.set_preview_text('Voyelles')"
    break
done



sleep 0.25
echo "MULTI@@SET@@set_active(0)@@_c1,_c2,"

if [[ -f engine ]]; then
    lecteur=`cat engine`
    if [[ "$lecteur" = "OdyO" ]]; then
        echo "SET@_audioplayer.set_active(4)"
    elif [[ "$lecteur" = "MPlayer" ]]; then
        echo "SET@_audioplayer.set_active(0)"
    elif [[ "$lecteur" = "Sox" ]]; then
        echo "SET@_audioplayer.set_active(1)"
    elif [[ "$lecteur" = "GStreamer" ]]; then
        echo "SET@_audioplayer.set_active(2)"
    elif [[ "$lecteur" = "VLC" ]]; then
        echo "SET@_audioplayer.set_active(3)"
    fi
else
    echo "SET@_audioplayer.set_active(0)"
fi

function fc1() { 
    sleep 0.25
    _c1_get_active_text=$1
    if [[ $_c1_get_active_text = "consonnes" || $_c1_get_active_text = "CONSONNES" ]]; then
        C_t=$C1_ 
        Ct=$C1
    else
        C_t=$C2_ 
        Ct=$C2
    fi
    if [[ $C_t = "" ]]; then
        echo "TREE@@LOAD@@t1@@$PWD/listes/$_c1_get_active_text"
        T1=$_c1_get_active_text
    else
        rm -rf $PWD/listes/$_c1_get_active_text-perso
        tmp1=""
        tm2=""
        tmp3=""
        cat $PWD/listes/$_c1_get_active_text | while read line; do
            tmp1=`echo $line | cut -d '|' -f1 `
            tmp2=`echo $line | cut -d '|' -f2 `
            tmp3=$Ct
            echo "$tmp1|$tmp2|$tmp3" >> $PWD/listes/$_c1_get_active_text-perso
            tmp1=""
            tm2=""
            tmp3=""
        done
        mv "$PWD/listes/$_c1_get_active_text-perso" "$PWD/listes/$_c1_get_active_text"
        echo "TREE@@LOAD@@t1@@$PWD/listes/$_c1_get_active_text"
        T1=$_c1_get_active_text
    fi
    testcasse=`echo $arg1 | grep -e '[A-Z]*'`
    if [[ $? -eq 1 ]]; then
        echo "TREE@@FINDSELECT@@t1@@0@@`echo $arg1 | tr '[:upper:]' '[:lower:]'`"
    else
        echo "TREE@@FINDSELECT@@t1@@0@@`echo $arg1 | tr '[:lower:]' '[:upper:]'`"
    fi
}
function _c1(){
    sleep 0.25
    echo "TREE@@CLEAR@@t1
ITER@@fc1 $1"
}

function fc2() { 
    sleep 0.25
    _c2_get_active_text=$1
    if [[ $_c2_get_active_text = "consonnes" || $_c2_get_active_text = "CONSONNES" ]]; then
        C_t=$C1_ 
        Ct=$C1
    else
        C_t=$C2_ 
        Ct=$C2
    fi
    if [[ $C_t = "" ]]; then
        echo "TREE@@LOAD@@t2@@$PWD/listes/$_c2_get_active_text"
        T2=$_c2_get_active_text
    else
        rm -rf $PWD/listes/$_c2_get_active_text-perso
        tmp1=""
        tm2=""
        tmp3=""
        cat $PWD/listes/$_c2_get_active_text | while read line; do
            tmp1=`echo $line | cut -d '|' -f1 `
            tmp2=`echo $line | cut -d '|' -f2 `
            tmp3=$Ct
            echo "$tmp1|$tmp2|$tmp3" >> $PWD/listes/$_c2_get_active_text-perso
            tmp1=""
            tm2=""
            tmp3=""
        done
        mv "$PWD/listes/$_c2_get_active_text-perso" "$PWD/listes/$_c2_get_active_text"
        echo "TREE@@LOAD@@t2@@$PWD/listes/$_c2_get_active_text"
        T2=$_c2_get_active_text
    fi
    testcasse=`echo $arg2 | grep -e '[A-Z]*'`
    if [[ $? -eq 1 ]]; then
        echo "TREE@@FINDSELECT@@t2@@0@@`echo $arg2 | tr '[:upper:]' '[:lower:]'`"
    else
        echo "TREE@@FINDSELECT@@t2@@0@@`echo $arg2 | tr '[:lower:]' '[:upper:]'`"
    fi
}

function _c2(){
    sleep 0.25
    echo "TREE@@CLEAR@@t2
ITER@@fc2 $1"
}


function _colorC1() {
    __C1=$@
    C1=""
    C1_=""
    _c1 $T1
    C1_=$__C1;
    C1=$C1_
    _c1 $T1
}

function _colorC2() {
    __C2=$@;
    C2=""
    C2_=""
    _c2 $T2
    C2_=$__C2
    C2=$C2_
    _c2 $T2
}

function modifyFont() {
    cat $PWD/listes/$1 | while read line; do
            tmp1=`echo $line | cut -d '|' -f1 `
            tmp2="$2"
            tmp3=`echo $line | cut -d '|' -f3 `
            echo "$tmp1|$tmp2|$tmp3" >> $PWD/listes/$1-perso
            tmp1=""
            tm2=""
            tmp3=""
    done
    mv "$PWD/listes/$1-perso" "$PWD/listes/$1"

}

function _fontC1() {
    result_font=$(echo $@ | sed 's/__/ /g')
    modifyFont "$T1" "$result_font"
    _c1 "$T1"
}

function _fontC2() {
    result_font=$(echo $@ | sed 's/__/ /g')
    modifyFont "$T2" "$result_font"
    _c2 "$T2"
}

function _audioplayer() {
    if [[ "$1" = "OdyO" ]]; then
        #./odyo.py play host=localhost:3309 &
        ENGINE="odyo"
    elif [[ "$1" = "MPlayer" ]]; then
        ENGINE="mplayer "
    elif [[ "$1" = "Sox" ]]; then
        ENGINE="play "
    elif [[ "$1" = "GStreamer" ]]; then
        ENGINE="gst-launch  playbin uri=file://"
    elif [[ "$1" = "VLC" ]]; then
        ENGINE="cvlc --play-and-stop --play-and-exit "
    fi
    echo $1 > engine
}


function t1() {
    num_l1=$(cut -d '@' -f1 <<< $1)
    arg1="$(cut -d '@' -f1 --complement <<< $@)"
    if [[ $C1_ = "" ]]; then
        C1="$(echo $arg1 |cut -d '|' -f3 )"
    fi
    arg1="$(echo $arg1 |cut -d '|' -f1 )"
    
    if [[ $arg1 == "Aucune" ]]; then
        arg1=""
    fi
#    echo 'ITER@@_t1' "$C1" "$arg1" "$arg2"
    echo "SET@_I1.set_from_file('$PWD/imgs/`UPPER $arg1`.png')"
     echo "SET@_L.set_markup('<span font=\"$FONTLABEL\" color=\"$C1\">$arg1</span><span font=\"$FONTLABEL\" color=\"$C2\">$arg2</span>')"
     echo "SET@_llire.set_markup('<span font=\"18\" >Écouter <b>\"$arg1$arg2\"</b></span>')"
}

function _t1() {
     echo "SET@_L.set_markup('<span font=\"$FONTLABEL\" color=\"$1\" >$2</span>')"
     echo "SET@_llire.set_markup('<span font=\"18\" >Écouter <b>\"$2$3\"</b></span>')"
}

function t2() {
    num_l2=$(cut -d '@' -f1 <<< $1)
    arg2="$(cut -d '@' -f1 --complement <<< $@)"
    if [[ $C2_ = "" ]]; then
        C2="$(echo $arg2 |cut -d '|' -f3 )"
    fi
    arg2="$(echo $arg2 |cut -d '|' -f1 )"
    
    if [[ $arg2 == "Aucune" ]]; then
        arg2=""
    fi
    echo "SET@_I2.set_from_file('$PWD/imgs/`UPPER $arg2`.png')"
    echo "SET@_L.set_markup('<span font=\"$FONTLABEL\" color=\"$C1\" >$arg1</span><span font=\"$FONTLABEL\" color=\"$C2\" >$arg2</span>')"
    echo "SET@_llire.set_markup('<span font=\"18\" >Écouter <b>\"$arg1$arg2\"</b></span>')"
}

function UPPER() {
    g1_=`echo $1 | tr '[:lower:]' '[:upper:]'`
#    arg1_=$arg1
#    arg2_=$arg2
    if [[ "$1" = "î" || "$1" = "ï" || "$1" = "é" || "$1" = "è" || "$1" = "ê" || "$1" = "à" || "$1" = "ô" || "$1" = "ç" ]]; then
        if [[ "$1" = "î" || "$1" = "ï" ]]; then
            g1_="I"
        elif [[ "$1" = "é" || "$1" = "è" || "$1" = "ê" ]]; then
            g1_="E"
        elif [[ "$1" = "à" || "$1" = "â" ]]; then
            g1_="A"
        elif [[ "$1" = "ô" ]]; then
            g1_="O"
        elif [[ "$1" = "ç" ]]; then
            g1_="C"
        fi
    fi
    echo $g1_
}

function _lire() {
    ##FIXME
    arg1_=`echo $arg1 | tr '[:lower:]' '[:upper:]'`
    arg2_=`echo $arg2 | tr '[:lower:]' '[:upper:]'`
#    arg1_=$arg1
#    arg2_=$arg2
    if [[ "$arg1" = "î" || "$arg1" = "ï" || "$arg1" = "é" || "$arg1" = "è" || "$arg1" = "ê" || "$arg1" = "à" || "$arg1" = "ô" || "$arg1" = "ç" ]]; then
        if [[ "$arg1" = "î" || "$arg1" = "ï" ]]; then
            if [[ "$arg1" = "î" ]]; then
                arg1_="Î"
            elif [[ "$arg1" = "ï" ]]; then
                arg1_="Ï"
            fi
        elif [[ "$arg1" = "é" || "$arg1" = "è" || "$arg1" = "ê" ]]; then
            if [[ "$arg1" = "é" ]]; then
                arg1_="É"
            elif [[ "$arg1" = "è" ]]; then
                arg1_="È"
            elif [[ "$arg1" = "ê" ]]; then
                arg1_="Ê"
            fi
        elif [[ "$arg1" = "à" || "$arg1" = "â" ]]; then
            if [[ "$arg1" = "à" ]]; then
                arg1_="À"
            elif [[ "$arg1" = "â" ]]; then
                arg1_="Â"
            fi
        elif [[ "$arg1" = "ô" ]]; then
            arg1_="Ô"
        elif [[ "$arg1" = "ç" ]]; then
            arg1_="Ç"
        fi
    fi
    
    if [[ "$arg2" = "î" || "$arg2" = "ï" || "$arg2" = "é" || "$arg2" = "è" || "$arg2" = "ê" || "$arg2" = "à" || "$arg2" = "ô" || "$arg2" = "ç" ]]; then
        if [[ "$arg2" = "î" || "$arg2" = "ï" ]]; then
            if [[ "$arg2" = "î" ]]; then
                arg2_="Î"
            elif [[ "$arg2" = "ï" ]]; then
                arg2_="Ï"
            fi
        elif [[ "$arg2" = "é" || "$arg2" = "è" || "$arg2" = "ê" ]]; then
            if [[ "$arg2" = "é" ]]; then
                arg2_="É"
            elif [[ "$arg2" = "è" ]]; then
                arg2_="È"
            elif [[ "$arg2" = "ê" ]]; then
                arg2_="Ê"
            fi
        elif [[ "$arg2" = "à" || "$arg2" = "â" ]]; then
            if [[ "$arg2" = "à" ]]; then
                arg2_="À"
            elif [[ "$arg2" = "â" ]]; then
                arg2_="Â"
            fi
        elif [[ "$arg2" = "ô" ]]; then
            arg2_="Ô"
        elif [[ "$arg2" = "ç" ]]; then
            arg2_="Ç"
        fi
    fi

    
    #exception pour le G et C
    if [[ "$arg1_" = "G" ]]; then
        
        if [[ "$arg2_" = "I" || "$arg2_" = "Î" || "$arg2_" = "Ï" || "$arg2_" = "E" || "$arg2_" = "É" || "$arg2_" = "È" || "$arg2_" = "EIN" || "$arg2_" = "EAU" || "$arg2_" = "EU" || "$arg2_" = "EN" || "$arg2_" = "IN" ]]; then
            arg1_="J"
        fi
    elif [[ "$arg1_" = "C" ]]; then
        
        if [[ "$arg2_" = "I" || "$arg2_" = "Î" || "$arg2_" = "Ï" || "$arg2_" = "E" || "$arg2_" = "É" || "$arg2_" = "È" || "$arg2_" = "EIN" || "$arg2_" = "EAU" || "$arg2_" = "EU" || "$arg2_" = "EN" || "$arg2_" = "IN" ]]; then
            arg1_="S"
        fi
    fi
    if [[ "$ENGINE" = "odyo" ]]; then
        sleep 1 && echo "SET@_L.set_markup('<span font=\"$FONTLABEL\" color=\"$C1\"><b>$arg1</b></span>   $arg2')" && echo "SET@_llire.set_markup('<span font=\"18\" >Écouter \"<b>$arg1</b>$arg2\"</span>')" && OdyO1 "$PWD/audios/$arg1_.ogg" && echo "SET@_L.set_markup('$arg1   <span font=\"$FONTLABEL\" color=\"$C2\"><b>$arg2</b></span >')" && echo "SET@_llire.set_markup('<span font=\"18\" >Écouter \"$arg1<b>$arg2</b>\"</span>')" && OdyO2 "$PWD/audios/$arg2_.ogg" && echo "SET@_L.set_markup('<span font=\"$FONTLABEL\" color=\"$C1\"><b>$arg1</b></span><span font=\"$FONTLABEL\" color=\"$C2\"><b>$arg2</b></span >')" && echo "SET@_llire.set_markup('<span font=\"18\" >Écouter <b>\"$arg1$arg2\"</b></span>')" && OdyO3 "$PWD/audios/$arg1_$arg2_.ogg" &
        NP="stop"
    else
        sleep 1 && echo "SET@_L.set_markup('<span font=\"$FONTLABEL\" color=\"$C1\"><b>$arg1</b></span>   $arg2')" && echo "SET@_llire.set_markup('<span font=\"18\" >Écouter \"<b>$arg1</b>$arg2\"</span>')" && $ENGINE"$PWD/audios/$arg1_.ogg" && echo "SET@_L.set_markup('$arg1   <span font=\"$FONTLABEL\" color=\"$C2\"><b>$arg2</b></span >')" && echo "SET@_llire.set_markup('<span font=\"18\" >Écouter \"$arg1<b>$arg2</b></span>\"')" && $ENGINE"$PWD/audios/$arg2_.ogg" && echo "SET@_L.set_markup('<span font=\"$FONTLABEL\" color=\"$C1\"><b>$arg1</b></span><span font=\"$FONTLABEL\" color=\"$C2\"><b>$arg2</b></span >')" && echo "SET@_llire.set_markup('<span font=\"18\" >Écouter <b>\"$arg1$arg2\"</b></span>')" && $ENGINE"$PWD/audios/$arg1_$arg2_.ogg" &
    fi
}

function OdyO1() {
    ./odyo.py play "$1" 0 0 host=localhost:$P1 &
    touch $PWD/current1
    startdelay=3
    while [[ -f $PWD/current1 ]]; do
        hms_time_preview=`./odyo.py position 0 0 host=localhost:$P1`
        ms_current=`echo "$hms_time_preview" | cut -d';' -f1`
        totalhms=`echo "$hms_time_preview" | cut -d'|' -f3 | cut -d';' -f1`
        if [[ "$ms_current" -eq "" ]]; then
                ./odyo.py stop 0 0 host=localhost:$P1 &
                rm -rf $PWD/current1
                P1=$((P1+3))
                break
        fi
        progress_fraction=`python -c "print $ms_current.000000000/$totalhms"`
        timepurcent=`echo $progress_fraction | cut -d'.' -f1`
        if [[ "$timepurcent" -eq "" ]]; then
                timepurcent=0
        fi
        if [[ $timepurcent -eq 1 ]]; then
            if [[ $startdelay -eq 0 ]]; then
                ./odyo.py stop 0 0 host=localhost:$P1 &
                rm -rf $PWD/current1
                P1=$((P1+3))
                break
            fi
            startdelay=$((startdelay-1))
        fi
        #sleep 0.1
    done
}
function OdyO2() {
    ./odyo.py play "$1" 0 0 host=localhost:$P2 &
    touch $PWD/current2
    startdelay=3
    while [[ -f $PWD/current2 ]]; do
        hms_time_preview=`./odyo.py position 0 0 host=localhost:$P2`
        ms_current=`echo "$hms_time_preview" | cut -d';' -f1`
        totalhms=`echo "$hms_time_preview" | cut -d'|' -f3 | cut -d';' -f1`
        if [[ "$ms_current" -eq "" ]]; then
                ./odyo.py stop 0 0 host=localhost:$P2 &
                rm -rf $PWD/current2
                P2=$((P2+3))
                break
        fi
        progress_fraction=`python -c "print $ms_current.000000000/$totalhms"`
        timepurcent=`echo $progress_fraction | cut -d'.' -f1`
        if [[ "$timepurcent" -eq "" ]]; then
                timepurcent=0
        fi
        if [[ $timepurcent -eq 1 ]]; then
            if [[ $startdelay -eq 0 ]]; then
                ./odyo.py stop 0 0 host=localhost:$P2 &
                rm -rf $PWD/current2
                P2=$((P2+3))
                break
            fi
            startdelay=$((startdelay-1))
        fi
        #sleep 0.1
    done
}
function OdyO3() {
    ./odyo.py play "$1" 0 0 host=localhost:$P3 &
    touch $PWD/current3
    startdelay=3
    while [[ -f $PWD/current3 ]]; do
        hms_time_preview=`./odyo.py position 0 0 host=localhost:$P3`
        ms_current=`echo "$hms_time_preview" | cut -d';' -f1`
        totalhms=`echo "$hms_time_preview" | cut -d'|' -f3 | cut -d';' -f1`
        if [[ "$ms_current" -eq "" ]]; then
                ./odyo.py stop 0 0 host=localhost:$P3 &
                rm -rf $PWD/current3
                P3=$((P3+3))
                break
                break
        fi
        progress_fraction=`python -c "print $ms_current.000000000/$totalhms"`
        timepurcent=`echo $progress_fraction | cut -d'.' -f1`
        if [[ "$timepurcent" -eq "" ]]; then
                timepurcent=0
        fi
        if [[ $timepurcent -eq 1 ]]; then
            if [[ $startdelay -eq 0 ]]; then
                ./odyo.py stop 0 0 host=localhost:$P3 &
                rm -rf $PWD/current3
                P3=$((P3+3))
                break
            fi
            startdelay=$((startdelay-1))
        fi
        #sleep 0.1
    done
}
##########################################################################################
while read ligne; do
    if [[ "$ligne" =~ GET@ ]]; then
      eval ${ligne#*@}
      echo "DEBUG => in boucle bash :" ${ligne#*@}
    else
      echo "DEBUG=> in bash NOT GET" $ligne
      $ligne
   fi 
done < <(while true; do
    read entree < $FIFO
    [[ "$entree" == "QuitNow" ]] && break
    echo $entree   
done)
exit
